module gitlab.com/pidrakin/go/templates

go 1.19

require (
	github.com/stretchr/testify v1.8.1
	gitlab.com/pidrakin/go/slices v0.0.2
	gitlab.com/pidrakin/go/tests v0.0.1
	golang.org/x/exp v0.0.0-20221114191408-850992195362
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
