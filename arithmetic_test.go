package templates

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/go/tests"
	"testing"
)

func Test(t *testing.T) {
	t.Run("add", func(t *testing.T) {
		res := (funcMap["add"].(func(...any) any))(1, 2, 3)
		assert.Equal(t, 6, res)
		parsed, err := Tprintf("{{- add 1 2 3 -}}", TemplateData{})
		tests.AssertNoError(t, err)
		assert.Equal(t, "6", parsed)
	})
	t.Run("sub", func(t *testing.T) {
		res := (funcMap["sub"].(func(...any) any))(10, 5)
		assert.Equal(t, 5, res)
		parsed, err := Tprintf("{{- sub 10 5 -}}", TemplateData{})
		tests.AssertNoError(t, err)
		assert.Equal(t, "5", parsed)
	})
	t.Run("mul", func(t *testing.T) {
		res := (funcMap["mul"].(func(...any) any))(2, 3, 4)
		assert.Equal(t, 24, res)
		parsed, err := Tprintf("{{- mul 2 3 4 -}}", TemplateData{})
		tests.AssertNoError(t, err)
		assert.Equal(t, "24", parsed)
	})
	t.Run("div", func(t *testing.T) {
		res := (funcMap["div"].(func(...any) any))(16, 4)
		assert.Equal(t, 4, res)
		parsed, err := Tprintf("{{- div 16 4 -}}", TemplateData{})
		tests.AssertNoError(t, err)
		assert.Equal(t, "4", parsed)
	})
	t.Run("mod", func(t *testing.T) {
		res := (funcMap["mod"].(func(...any) any))(3, 2)
		assert.Equal(t, 1, res)
		parsed, err := Tprintf("{{- mod 3 2 -}}", TemplateData{})
		tests.AssertNoError(t, err)
		assert.Equal(t, "1", parsed)
	})
	t.Run("min", func(t *testing.T) {
		res := (funcMap["min"].(func(...any) any))(1, 9, 2, 8, 3, 7, 4, 6, 5)
		assert.Equal(t, 1, res)
		parsed, err := Tprintf("{{- min 1 9 2 8 3 7 4 6 5 -}}", TemplateData{})
		tests.AssertNoError(t, err)
		assert.Equal(t, "1", parsed)
	})
	t.Run("max", func(t *testing.T) {
		res := (funcMap["max"].(func(...any) any))(1, 9, 2, 8, 3, 7, 4, 6, 5)
		assert.Equal(t, 9, res)
		parsed, err := Tprintf("{{- max 1 9 2 8 3 7 4 6 5 -}}", TemplateData{})
		tests.AssertNoError(t, err)
		assert.Equal(t, "9", parsed)
	})
}
