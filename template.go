package templates

import (
	"bytes"
	"fmt"
	"gitlab.com/pidrakin/go/slices"
	"reflect"
	"text/template"
)

func isset(name string, data interface{}) bool {
	v := reflect.ValueOf(data)
	if v.Kind() == reflect.Ptr {
		v = v.Elem()
	}
	if v.Kind() == reflect.Map {
		keys := slices.Map(v.MapKeys(), func(t reflect.Value) string {
			return t.Interface().(string)
		})
		return slices.Contains(keys, name)
	}
	if v.Kind() != reflect.Struct {
		return false
	}
	return v.FieldByName(name).IsValid()
}

var (
	funcMap = template.FuncMap{}
)

func RegisterFunction(name string, f any) {
	funcMap[name] = f
}

func init() {
	RegisterFunction("isset", isset)
	RegisterFunction("printf", fmt.Sprintf)
}

type TemplateData map[string]interface{}

func Tprintf(tmpl string, data TemplateData) (string, error) {
	t, err := template.New("templates").Funcs(funcMap).Parse(tmpl)
	if err != nil {
		return "", err
	}
	buf := &bytes.Buffer{}
	if err = t.Execute(buf, data); err != nil {
		return "", err
	}
	return buf.String(), nil
}
