package templates

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_FileName(t *testing.T) {
	name := FileNameBase("foo")
	assert.Equal(t, "foo", name)
	name = FileNameBase("foo.ext")
	assert.Equal(t, "foo", name)
	name = FileNameBase("/bar/foo.ext")
	assert.Equal(t, "foo", name)
}

func Test_ToUpperSnakeCase(t *testing.T) {
	transformed := ToUpperSnakeCase("abc")
	assert.Equal(t, "ABC", transformed)

	transformed = ToUpperSnakeCase("abcDe")
	assert.Equal(t, "ABC_DE", transformed)
}
