package templates

import (
	"path/filepath"
	"regexp"
	"strings"
)

func init() {
	RegisterFunction("lower", strings.ToLower)
	RegisterFunction("upper", strings.ToUpper)
	RegisterFunction("join", strings.Join)
	RegisterFunction("split", strings.Split)
	RegisterFunction("hasPrefix", strings.HasPrefix)
	RegisterFunction("hasSuffix", strings.HasSuffix)
	RegisterFunction("trim", strings.Trim)
	RegisterFunction("trimPrefix", strings.TrimPrefix)
	RegisterFunction("trimSuffix", strings.TrimSuffix)
	RegisterFunction("trimLeft", strings.TrimLeft)
	RegisterFunction("trimRight", strings.TrimRight)
	RegisterFunction("repeat", strings.Repeat)
	RegisterFunction("replace", strings.Replace)
	RegisterFunction("replaceAll", strings.ReplaceAll)
	RegisterFunction("upperSnakecase", ToUpperSnakeCase)
}

func FileNameBase(path string) string {
	base := filepath.Dir(path)
	filename, _ := filepath.Rel(base, path)
	return strings.TrimSuffix(filename, filepath.Ext(filename))
}

var matchFirstCap = regexp.MustCompile("(.)([A-Z][a-z]+)")
var matchAllCap = regexp.MustCompile("([a-z0-9])([A-Z])")

func ToUpperSnakeCase(str string) string {
	snake := matchFirstCap.ReplaceAllString(str, "${1}_${2}")
	snake = matchAllCap.ReplaceAllString(snake, "${1}_${2}")
	return strings.ToUpper(snake)
}
