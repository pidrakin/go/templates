package templates

import "reflect"

func init() {
	RegisterFunction("entries", entries)
	RegisterFunction("dict", dict)
}

type Entry struct {
	Key   string
	Value interface{}
}

func entries(data interface{}) []Entry {
	var result []Entry
	v := reflect.ValueOf(data)
	if v.Kind() != reflect.Map {
		return result
	}
	for _, k := range v.MapKeys() {
		result = append(result, Entry{
			Key:   k.Interface().(string),
			Value: v.MapIndex(k).Interface(),
		})
	}
	return result
}

func dict(params ...any) map[string]any {
	result := make(map[string]any)
	if len(params)%2 != 0 {
		return result
	}
	for i := 0; i < len(params); i += 2 {
		result[params[i].(string)] = params[i+1]
	}
	return result
}
