package templates

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/go/tests"
	"testing"
)

func TestTprintf(t *testing.T) {
	data := TemplateData{
		"var1": "foobar",
		"var2": 1,
		"var3": true,
		"var4": struct {
			Field string
		}{Field: "bazbar"},
	}
	t.Run("succeeds", func(t *testing.T) {
		parsed, err := Tprintf("{{ .var1 }} {{ .var2 }} {{ .var3 }} {{ .var4.Field }}", data)
		tests.AssertNoError(t, err)
		expected := "foobar 1 true bazbar"
		assert.Equal(t, expected, parsed)
	})
	t.Run("fails with panic", func(t *testing.T) {
		parsed, err := Tprintf("{{ if .foobar }}{{ .var1 }}", data)
		assert.Error(t, err, "template: templates:1: unexpected EOF")
		assert.Empty(t, parsed)
	})
	t.Run("fails with empty template", func(t *testing.T) {
		parsed, err := Tprintf("", data)
		tests.AssertNoError(t, err)
		assert.Empty(t, parsed)
	})
}
