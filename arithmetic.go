package templates

import (
	"gitlab.com/pidrakin/go/slices"
	"golang.org/x/exp/constraints"
	"math"
	"reflect"
)

func init() {
	RegisterFunction("add", add)
	RegisterFunction("sub", sub)
	RegisterFunction("mul", mul)
	RegisterFunction("div", div)
	RegisterFunction("mod", mod)
	RegisterFunction("min", min)
	RegisterFunction("max", max)
}

type functions struct {
	Add any
	Sub any
	Mul any
	Div any
	Mod any
	Min any
	Max any
}

func fun(params ...any) functions {
	if len(params) == 0 {
		panic("need at least 1 argument")
	}
	f := functions{}
	switch params[0].(type) {
	case int:
		f.Add = addTyped[int]
		f.Sub = subTyped[int]
		f.Mul = mulTyped[int]
		f.Div = divTyped[int]
		f.Mod = modTyped[int]
		f.Min = minTyped[int]
		f.Max = maxTyped[int]
	case int8:
		f.Add = addTyped[int8]
		f.Sub = subTyped[int8]
		f.Mul = mulTyped[int8]
		f.Div = divTyped[int8]
		f.Mod = modTyped[int8]
		f.Min = minTyped[int8]
		f.Max = maxTyped[int8]
	case int16:
		f.Add = addTyped[int16]
		f.Sub = subTyped[int16]
		f.Mul = mulTyped[int16]
		f.Div = divTyped[int16]
		f.Mod = modTyped[int16]
		f.Min = minTyped[int16]
		f.Max = maxTyped[int16]
	case int32:
		f.Add = addTyped[int32]
		f.Sub = subTyped[int32]
		f.Mul = mulTyped[int32]
		f.Div = divTyped[int32]
		f.Mod = modTyped[int32]
		f.Min = minTyped[int32]
		f.Max = maxTyped[int32]
	case int64:
		f.Add = addTyped[int64]
		f.Sub = subTyped[int64]
		f.Mul = mulTyped[int64]
		f.Div = divTyped[int64]
		f.Mod = modTyped[int64]
		f.Min = minTyped[int64]
		f.Max = maxTyped[int64]
	case uint:
		f.Add = addTyped[uint]
		f.Sub = subTyped[uint]
		f.Mul = mulTyped[uint]
		f.Div = divTyped[uint]
		f.Mod = modTyped[uint]
		f.Min = minTyped[uint]
		f.Max = maxTyped[uint]
	case uint8:
		f.Add = addTyped[uint8]
		f.Sub = subTyped[uint8]
		f.Mul = mulTyped[uint8]
		f.Div = divTyped[uint8]
		f.Mod = modTyped[uint8]
		f.Min = minTyped[uint8]
		f.Max = maxTyped[uint8]
	case uint16:
		f.Add = addTyped[uint16]
		f.Sub = subTyped[uint16]
		f.Mul = mulTyped[uint16]
		f.Div = divTyped[uint16]
		f.Mod = modTyped[uint16]
		f.Min = minTyped[uint16]
		f.Max = maxTyped[uint16]
	case uint32:
		f.Add = addTyped[uint32]
		f.Sub = subTyped[uint32]
		f.Mul = mulTyped[uint32]
		f.Div = divTyped[uint32]
		f.Mod = modTyped[uint32]
		f.Min = minTyped[uint32]
		f.Max = maxTyped[uint32]
	case uint64:
		f.Add = addTyped[uint64]
		f.Sub = subTyped[uint64]
		f.Mul = mulTyped[uint64]
		f.Div = divTyped[uint64]
		f.Mod = modTyped[uint64]
		f.Min = minTyped[uint64]
		f.Max = maxTyped[uint64]
	case float32:
		f.Add = addTyped[float32]
		f.Sub = subTyped[float32]
		f.Mul = mulTyped[float32]
		f.Div = divTyped[float32]
		f.Mod = modTypedFloat
		f.Min = minTyped[float32]
		f.Max = maxTyped[float32]
	case float64:
		f.Add = addTyped[float64]
		f.Sub = subTyped[float64]
		f.Mul = mulTyped[float64]
		f.Div = divTyped[float64]
		f.Mod = modTypedFloat
		f.Min = minTyped[float64]
		f.Max = maxTyped[float64]
	}
	return f
}

func validateArgs[T any](params ...T) {
	var types []reflect.Type
	for _, p := range params {
		types = append(types, reflect.TypeOf(p))
	}

	same := slices.Reduce(types, true, func(p reflect.Type, index int, slice []reflect.Type, result bool) bool {
		if index == 0 {
			return true
		}
		return result && slice[index-1] == p
	})

	if !same {
		panic("types should be the same")
	}
}

func wrapper(name string, params ...any) any {
	f := fun(params...)
	var vs []reflect.Value
	for _, p := range params {
		vs = append(vs, reflect.ValueOf(p))
	}
	res := reflect.ValueOf(f).FieldByName(name).Elem().Call(vs)[0]
	return res.Interface()
}

func add(params ...any) any {
	return wrapper("Add", params...)
}

func addTyped[T constraints.Ordered](a T, b T, rest ...T) T {
	params := append([]T{a, b}, rest...)
	validateArgs(params...)
	res := a + b
	for _, e := range rest {
		res += e
	}
	return res
}

func sub(params ...any) any {
	return wrapper("Sub", params...)
}

func subTyped[T constraints.Integer | constraints.Float](a T, b T) T {
	return a - b
}

func mul(params ...any) any {
	return wrapper("Mul", params...)
}

func mulTyped[T constraints.Integer | constraints.Float](a T, b T, rest ...T) T {
	res := a * b
	for _, e := range rest {
		res *= e
	}
	return res
}

func div(params ...any) any {
	return wrapper("Div", params...)
}

func divTyped[T constraints.Integer | constraints.Float](a T, b T) T {
	return a / b
}

func mod(params ...any) any {
	return wrapper("Mod", params...)
}

func modTyped[T constraints.Integer](a T, b T) T {
	return a % b
}

func modTypedFloat(a float64, b float64) float64 {
	return math.Mod(a, b)
}

func min(params ...any) any {
	return wrapper("Min", params...)
}

func minTyped[T constraints.Ordered](a T, b T, rest ...T) T {
	var res T
	if a < b {
		res = a
	} else {
		res = b
	}
	for _, e := range rest {
		if e < res {
			res = e
		}
	}
	return res
}

func max(params ...any) any {
	return wrapper("Max", params...)
}

func maxTyped[T constraints.Ordered](a T, b T, rest ...T) T {
	var res T
	if a > b {
		res = a
	} else {
		res = b
	}
	for _, e := range rest {
		if e > res {
			res = e
		}
	}
	return res
}
